package com.limox.jesus.quiniela.Model;

import java.util.ArrayList;

/**
 * Created by jesus on 11/01/17.
 */

public class Resultado {
    private ArrayList<String> resultado;
    private int[] victoriasPorPosticion;

    public Resultado() {
        this.resultado = new ArrayList<>();
        this.victoriasPorPosticion = new int[15];
    }

    public ArrayList<String> getResultado() {
        return resultado;
    }

    public void setResultado(ArrayList<String> resultado) {
        this.resultado = resultado;
    }

    public int[] getVictoriasPorPosticion() {
        return victoriasPorPosticion;
    }

    public void setVictoriasPorPosticion(int[] victoriasPorPosticion) {
        this.victoriasPorPosticion = victoriasPorPosticion;
    }
}
