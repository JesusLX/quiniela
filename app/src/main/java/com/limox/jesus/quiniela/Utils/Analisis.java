package com.limox.jesus.quiniela.Utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import com.limox.jesus.quiniela.Model.Quinielas;
import com.limox.jesus.quiniela.Model.Resultado;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by jesus on 6/01/17.
 */

public class Analisis {

    public static Quinielas getLastQuiniela(File lasQuinielas, Context mContext) throws XmlPullParserException, IOException {

        int vueltas = 0;
        Quinielas tmpQuiniela = null;
        Quinielas lastQuiniela = null;
        String tag;
        boolean dentroQuinielista = false;
        boolean dentroQuiniela = false;
        boolean dentroPartit = false;

        //new Connect(mContext).upload(sb.toString(),"TODO.txt");

        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(lasQuinielas));
        int eventType = xpp.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    tag = xpp.getName();
                    if (tag.equals("quinielista"))
                        dentroQuinielista = true;
                    if (tag.equals("quiniela")) {
                        dentroQuiniela = true;
                        tmpQuiniela = new Quinielas();
                        tmpQuiniela.setEl15(xpp.getAttributeValue(null, "el15"));
                        tmpQuiniela.setEl14(xpp.getAttributeValue(null, "el14"));
                        tmpQuiniela.setEl13(xpp.getAttributeValue(null, "el13"));
                        tmpQuiniela.setEl12(xpp.getAttributeValue(null, "el12"));
                        tmpQuiniela.setEl11(xpp.getAttributeValue(null, "el11"));
                        tmpQuiniela.setEl10(xpp.getAttributeValue(null, "el10"));
                    }
                    if (tag.equals("partit")) {
                        dentroPartit = true;
                        vueltas++;

                        if (!xpp.getAttributeValue(null, "sig").isEmpty())
                            tmpQuiniela.addSig(xpp.getAttributeValue(null, "sig"));
                        else
                            return lastQuiniela;

                    }
                case XmlPullParser.END_TAG:
                    if (xpp.getName().equals("quiniela")) {
                        dentroQuiniela = false;
                        if (!tmpQuiniela.getSig().isEmpty()) {
                            lastQuiniela = tmpQuiniela;
                        }

                    }
            }
            eventType = xpp.next();
        }
        vueltas++;
        int algo = vueltas;
        Log.e("Vueltas", String.valueOf(vueltas));
        return lastQuiniela;
    }

    public static Resultado getResults(Quinielas quinielas, File apuestas,Context mContext) throws FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(apuestas));
        String line;

        int increment = 0;
        int victories = 0;
        String subst;

        Resultado resultado = new Resultado();

        try {
            while ((line = br.readLine()) != null) {
                for (int i = 0; i < line.length() - 1; i++) {
                    if (i == line.length() - 2)
                        increment = 1;
                    subst = line.substring(i, i + 1 + increment);
                    if (subst.equals(quinielas.getSig().get(i)) && (i < line.length() - 2)) {
                        victories++;
                    }
                    if (i == line.length() - 2) {
                        if (victories == 14) {
                            if (subst.equals(quinielas.getSig().get(i))) {
                                victories++;
                            }
                        }
                    }

                }
                if (victories != 0)
                    resultado.getVictoriasPorPosticion()[victories-1]++;
                victories = 0;
                increment = 0;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext,"Error con el fichero de las apuestas",Toast.LENGTH_LONG).show();
        }
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
            return resultado;
        }


    public static String escribirJSON(Resultado resultado, String fichero) throws IOException, JSONException {
        OutputStreamWriter out;
        File miFichero;
        JSONObject objeto, rss, item;
        JSONArray lista;
        miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fichero);
        out = new FileWriter(miFichero);
        //crear objeto JSON
        objeto = new JSONObject();

        item = new JSONObject();

        for (int i = 9; i < resultado.getVictoriasPorPosticion().length; i++) {
            item.put("victory" + (i + 1), resultado.getVictoriasPorPosticion()[i]);
        }

        objeto.put("resultados", item);

        out.write(objeto.toString(4)); //tabulación de 4 caracteres
        out.flush();
        out.close();
        Log.i("info", objeto.toString());
        return objeto.toString(4);
    }

    public static String crearXML(Resultado resultado, String fileName) throws IOException {
        FileOutputStream fout;
        fout = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName));
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(fout, "UTF-8");
        serializer.startDocument(null, true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true); //poner tabulación
        serializer.startTag(null, "results");
        for (int i = 9; i < resultado.getVictoriasPorPosticion().length; i++) {
            serializer.startTag(null, "partit");
            serializer.attribute(null, "num", String.valueOf(i+1));
            serializer.attribute(null, "wins", String.valueOf(resultado.getVictoriasPorPosticion()[i]));
            serializer.endTag(null, "partit");
        }
        serializer.endTag(null, "results");
        serializer.endDocument();
        serializer.flush();
        fout.close();
        BufferedReader br = new BufferedReader(new FileReader(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName)));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }

    public static void crearXML(Quinielas quiniela, Context mContext) throws IOException {
        FileOutputStream fout;
        fout = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "quiniela.xml"));
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(fout, "UTF-8");
        serializer.startDocument(null, true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true); //poner tabulación
        serializer.startTag(null, "quiniela");
        serializer.attribute(null, "el10", String.valueOf(quiniela.getEl10()));
        serializer.attribute(null, "el11", String.valueOf(quiniela.getEl11()));
        serializer.attribute(null, "el12", String.valueOf(quiniela.getEl12()));
        serializer.attribute(null, "el13", String.valueOf(quiniela.getEl13()));
        serializer.attribute(null, "el14", String.valueOf(quiniela.getEl14()));
        serializer.attribute(null, "el15", String.valueOf(quiniela.getEl15()));

        for (int i = 0; i < quiniela.getSig().size(); i++) {
            serializer.startTag(null, "partit");
            serializer.attribute(null, "num", String.valueOf(i + 1));
            serializer.attribute(null, "sig", String.valueOf(quiniela.getSig().get(i)));
            serializer.endTag(null, "partit");
        }
        serializer.endTag(null, "quiniela");
        serializer.endDocument();
        serializer.flush();
        fout.close();
        BufferedReader br = new BufferedReader(new FileReader(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/quiniela.xml")));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        new Connect(mContext).upload(sb.toString(), "quiniela.xml");
    }

    public static void escribirJSON(Quinielas quinielas, Context mContext) throws IOException, JSONException {
        OutputStreamWriter out;
        File miFichero;
        JSONObject objeto, rss, item;
        JSONArray lista;
        miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "quiniela.json");
        out = new FileWriter(miFichero);
        //crear objeto JSON
        objeto = new JSONObject();

        item = new JSONObject();
        lista = new JSONArray();
        item.put("el10", quinielas.getEl10());
        item.put("el11", quinielas.getEl11());
        item.put("el12", quinielas.getEl12());
        item.put("el13", quinielas.getEl13());
        item.put("el14", quinielas.getEl14());
        item.put("el15", quinielas.getEl15());
        for (int i = 0; i < quinielas.getSig().size(); i++) {
            lista.put(i + 1, quinielas.getSig().get(i));
        }
        item.put("sig", lista);

        objeto.put("quiniela", item);

        out.write(objeto.toString(4)); //tabulación de 4 caracteres
        out.flush();
        out.close();
        Log.i("info", objeto.toString());
        BufferedReader br = new BufferedReader(new FileReader(miFichero));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        new Connect(mContext).upload(sb.toString(), "quiniela.json");
    }
}
