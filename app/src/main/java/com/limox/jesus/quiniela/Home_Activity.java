package com.limox.jesus.quiniela;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

import com.limox.jesus.quiniela.Utils.Connect;

public class Home_Activity extends AppCompatActivity {

    Button btnCalcular;
    EditText edtApuestas;
    EditText edtResultados;
    EditText edtAciertosYPremios;
    RadioButton rbtnJson;
    Connect mConnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        edtAciertosYPremios = (EditText) findViewById(R.id.edtAcYPre);
        edtResultados = (EditText) findViewById(R.id.edtResultados);
        edtApuestas = (EditText) findViewById(R.id.edtApuestas);
        rbtnJson = (RadioButton) findViewById(R.id.rBtnJSON);
        mConnect = new Connect(Home_Activity.this);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mConnect.descargarQuinielas(edtApuestas.getText().toString(),rbtnJson.isChecked(),edtApuestas.getText().toString(),edtAciertosYPremios.getText().toString());
            }
        });
    }
}
