package com.limox.jesus.quiniela.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.limox.jesus.quiniela.Model.Quinielas;
import com.limox.jesus.quiniela.Model.Resultado;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


/**
 * Created by jesus on 13/01/17.
 */

public class Connect {
    private Context mContext;
    private Quinielas mQuiniela;
    private Resultado mResultado;
    final static String WEB_UPLOAD = "http://alumno.club/superior/jesusp/subir2.php";
    final static String APUESTAS = "http://alumno.club/superior/jesusp/apuestas.txt";
    final static String QUINIELAS = "https://www.quinielista.es/xml/temporada.asp";

    public Connect(Context mContext) {
        this.mContext = mContext;
    }

    public boolean upload(String file, final String filename) throws FileNotFoundException {
        final ProgressDialog progress = new ProgressDialog(mContext);
        RequestParams params = new RequestParams();
        params.put("contenido", file);
        params.put("filename", filename);
        RestClient.post(WEB_UPLOAD, params, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                // called before request is started
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Subiendo fichero . . .");
                //progreso.setCancelable(false);
                progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(mContext, true);
                    }
                });
                progress.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(mContext, "Fallo al subir el fallo", Toast.LENGTH_SHORT).show();
                progress.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Toast.makeText(mContext, filename + " subido", Toast.LENGTH_SHORT).show();
                progress.dismiss();
            }
        });
        return true;
    }

    public void descargarApuestas(String web, final boolean json, final String fileName) {
        final ProgressDialog progreso = new ProgressDialog(mContext);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "apuestas.txt");
        try {
            miFichero.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        RestClient.get(web, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Log.e("dismiss", throwable.getMessage());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();

                try {
                    mResultado = Analisis.getResults(mQuiniela, file, mContext);
                    if (json)
                        upload(Analisis.escribirJSON(mResultado, fileName), fileName);
                    else
                        upload(Analisis.crearXML(mResultado, fileName), fileName);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "Error al comparar las apuestas", Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

        });

    }

    public void descargarQuinielas(String web, final boolean isJson, final String apuestas, final String fileResultName) {
        final ProgressDialog progreso = new ProgressDialog(mContext);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "quinielas.xml");
        try {
            miFichero.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        RestClient.get(QUINIELAS, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Log.e("dismiss", throwable.getMessage());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                try {
                    progreso.dismiss();
                    mQuiniela = Analisis.getLastQuiniela(file, mContext);
                    mQuiniela.getSig();
                    if (isJson) {
                        Analisis.escribirJSON(mQuiniela, mContext);
                    } else {
                        Analisis.crearXML(mQuiniela, mContext);
                    }
                    descargarApuestas(apuestas, isJson, fileResultName);
                } catch (XmlPullParserException | IOException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

        });

    }
}
