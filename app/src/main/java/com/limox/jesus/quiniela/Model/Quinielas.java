package com.limox.jesus.quiniela.Model;

import java.util.ArrayList;

/**
 * Created by jesus on 11/01/17.
 */

public class Quinielas {
    private String el10;
    private String el11;
    private String el12;
    private String el13;
    private String el14;
    private String el15;

    private ArrayList<String> sig;

    public Quinielas() {
        sig = new ArrayList<>();
    }

    public String getEl10() {
        return el10;
    }

    public void setEl10(String el10) {
        this.el10 = el10;
    }

    public String getEl11() {
        return el11;
    }

    public void setEl11(String el11) {
        this.el11 = el11;
    }

    public String getEl12() {
        return el12;
    }

    public void setEl12(String el12) {
        this.el12 = el12;
    }

    public String getEl13() {
        return el13;
    }

    public void setEl13(String el13) {
        this.el13 = el13;
    }

    public String getEl14() {
        return el14;
    }

    public void setEl14(String el14) {
        this.el14 = el14;
    }

    public String getEl15() {
        return el15;
    }

    public void setEl15(String el15) {
        this.el15 = el15;
    }

    public ArrayList<String> getSig() {
        return sig;
    }

    public void setSig(ArrayList<String> sig) {
        this.sig = sig;
    }
    public void addSig(String sig) {
        this.sig.add(sig);
    }
}
